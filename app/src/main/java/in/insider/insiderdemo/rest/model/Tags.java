package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class Tags {
    @Json(name = "priority")
    private int priority;
    @Json(name = "tag_id")
    private String tag_id;
    @Json(name = "_id")
    private String _id;
    @Json(name = "is_primary_interest")
    private boolean is_primary_interest;
    @Json(name = "is_pick")
    private boolean is_pick;
    @Json(name = "is_carousel")
    private boolean is_carousel;
    @Json(name = "is_featured")
    private boolean is_featured;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isIs_primary_interest() {
        return is_primary_interest;
    }

    public void setIs_primary_interest(boolean is_primary_interest) {
        this.is_primary_interest = is_primary_interest;
    }

    public boolean isIs_pick() {
        return is_pick;
    }

    public void setIs_pick(boolean is_pick) {
        this.is_pick = is_pick;
    }

    public boolean isIs_carousel() {
        return is_carousel;
    }

    public void setIs_carousel(boolean is_carousel) {
        this.is_carousel = is_carousel;
    }

    public boolean isIs_featured() {
        return is_featured;
    }

    public void setIs_featured(boolean is_featured) {
        this.is_featured = is_featured;
    }
}
