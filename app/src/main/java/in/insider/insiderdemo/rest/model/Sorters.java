package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.List;

public class Sorters {
    @Json(name = "Events")
    private List<DisplayKey> events;
    @Json(name = "Food")
    private List<DisplayKey> food;
    @Json(name = "Travel")
    private List<DisplayKey> travel;
    @Json(name = "Workshops")
    private List<DisplayKey> workshops;
    @Json(name = "BARgain")
    private List<DisplayKey> bARgain;
    @Json(name = "Festivals")
    private List<DisplayKey> festivals;
    @Json(name = "Theatre")
    private List<DisplayKey> theatre;

    public List<DisplayKey> getEvents() {
        return events;
    }

    public void setEvents(List<DisplayKey> events) {
        this.events = events;
    }

    public List<DisplayKey> getFood() {
        return food;
    }

    public void setFood(List<DisplayKey> food) {
        this.food = food;
    }

    public List<DisplayKey> getTravel() {
        return travel;
    }

    public void setTravel(List<DisplayKey> travel) {
        this.travel = travel;
    }

    public List<DisplayKey> getWorkshops() {
        return workshops;
    }

    public void setWorkshops(List<DisplayKey> workshops) {
        this.workshops = workshops;
    }

    public List<DisplayKey> getbARgain() {
        return bARgain;
    }

    public void setbARgain(List<DisplayKey> bARgain) {
        this.bARgain = bARgain;
    }

    public List<DisplayKey> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<DisplayKey> festivals) {
        this.festivals = festivals;
    }

    public List<DisplayKey> getTheatre() {
        return theatre;
    }

    public void setTheatre(List<DisplayKey> theatre) {
        this.theatre = theatre;
    }
}
