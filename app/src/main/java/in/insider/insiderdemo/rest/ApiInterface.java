package in.insider.insiderdemo.rest;

import android.arch.lifecycle.LiveData;

import in.insider.insiderdemo.rest.model.ApiResponse;
import in.insider.insiderdemo.rest.model.BaseResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("home")
    LiveData<ApiResponse<BaseResponse>> getResponse(@Query("norm") int norm, @Query("filterBy") String filterBy, @Query("city") String city);
}
