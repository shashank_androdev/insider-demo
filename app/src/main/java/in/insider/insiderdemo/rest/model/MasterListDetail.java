package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.List;

public class MasterListDetail {
    @Json(name = "_id")
    private String _id;
    @Json(name = "min_show_start_time")
    private Long min_show_start_time;
    @Json(name = "name")
    private String name;
    @Json(name = "type")
    private String type;
    @Json(name = "slug")
    private String slug;
    @Json(name = "horizontal_cover_image")
    private String horizontal_cover_image;
    @Json(name = "vertical_cover_image")
    private String vertical_cover_image;
    @Json(name = "tags")
    private List<Tags> tags;
    @Json(name = "city")
    private String city;
    @Json(name = "venue_id")
    private String venue_id;
    @Json(name = "venue_name")
    private String venue_name;
    @Json(name = "venue_date_string")
    private String venue_date_string;
    @Json(name = "is_rsvp")
    private boolean is_rsvp;
    @Json(name = "category_id")
    private CategoryId category_id;
    @Json(name = "group_id")
    private CategoryId group_id;
    @Json(name = "event_state")
    private String event_state;
    @Json(name = "price_display_string")
    private String price_display_string;
    @Json(name = "model")
    private String model;
    @Json(name = "applicable_filters")
    private String[] applicable_filters;
    @Json(name = "min_price")
    private int min_price;
    @Json(name = "isInjected")
    private boolean isInjected;
    @Json(name = "injectedPosition")
    private int injectedPosition;
    @Json(name = "popularity_score")
    private int popularity_score;
    @Json(name = "map_link")
    private String map_link;
    @Json(name = "priority")
    private int priority;
    @Json(name = "display_details")
    private DisplayDetails display_details;

    public String getMap_link() {
        return map_link;
    }

    public void setMap_link(String map_link) {
        this.map_link = map_link;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public DisplayDetails getDisplay_details() {
        return display_details;
    }

    public void setDisplay_details(DisplayDetails display_details) {
        this.display_details = display_details;
    }

    public String getVertical_cover_image() {
        return vertical_cover_image;
    }

    public void setVertical_cover_image(String vertical_cover_image) {
        this.vertical_cover_image = vertical_cover_image;
    }

    public boolean isInjected() {
        return isInjected;
    }

    public void setInjected(boolean injected) {
        isInjected = injected;
    }

    public int getInjectedPosition() {
        return injectedPosition;
    }

    public void setInjectedPosition(int injectedPosition) {
        this.injectedPosition = injectedPosition;
    }

    public int getPopularity_score() {
        return popularity_score;
    }

    public void setPopularity_score(int popularity_score) {
        this.popularity_score = popularity_score;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Long getMin_show_start_time() {
        return min_show_start_time;
    }

    public void setMin_show_start_time(Long min_show_start_time) {
        this.min_show_start_time = min_show_start_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getHorizontal_cover_image() {
        return horizontal_cover_image;
    }

    public void setHorizontal_cover_image(String horizontal_cover_image) {
        this.horizontal_cover_image = horizontal_cover_image;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(String venue_id) {
        this.venue_id = venue_id;
    }

    public String getVenue_name() {
        return venue_name;
    }

    public void setVenue_name(String venue_name) {
        this.venue_name = venue_name;
    }

    public String getVenue_date_string() {
        return venue_date_string;
    }

    public void setVenue_date_string(String venue_date_string) {
        this.venue_date_string = venue_date_string;
    }

    public boolean isIs_rsvp() {
        return is_rsvp;
    }

    public void setIs_rsvp(boolean is_rsvp) {
        this.is_rsvp = is_rsvp;
    }

    public CategoryId getCategory_id() {
        return category_id;
    }

    public void setCategory_id(CategoryId category_id) {
        this.category_id = category_id;
    }

    public CategoryId getGroup_id() {
        return group_id;
    }

    public void setGroup_id(CategoryId group_id) {
        this.group_id = group_id;
    }

    public String getEvent_state() {
        return event_state;
    }

    public void setEvent_state(String event_state) {
        this.event_state = event_state;
    }

    public String getPrice_display_string() {
        return price_display_string;
    }

    public void setPrice_display_string(String price_display_string) {
        this.price_display_string = price_display_string;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String[] getApplicable_filters() {
        return applicable_filters;
    }

    public void setApplicable_filters(String[] applicable_filters) {
        this.applicable_filters = applicable_filters;
    }

    public int getMin_price() {
        return min_price;
    }

    public void setMin_price(int min_price) {
        this.min_price = min_price;
    }
}
