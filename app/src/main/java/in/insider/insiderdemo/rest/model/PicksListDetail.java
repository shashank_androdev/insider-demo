package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.List;

public class PicksListDetail {
    @Json(name = "_id")
    private String _id;
    @Json(name = "timestamp_added")
    private String timestamp_added;
    @Json(name = "timestamp_modified")
    private String timestamp_modified;
    @Json(name = "slug")
    private String slug;
    @Json(name = "title")
    private String title;
    @Json(name = "summary")
    private String summary;
    @Json(name = "headerImage")
    private String headerImage;
    @Json(name = "horizontalImageCover")
    private String horizontalImageCover;
    @Json(name = "category_id")
    private CategoryId category_id;
    @Json(name = "group_id")
    private CategoryId group_id;
    @Json(name = "action_button_text")
    private String action_button_text;
    @Json(name = "action_button_link")
    private String action_button_link;
    @Json(name = "tags")
    private List<Tags> tags;
    @Json(name = "__v")
    private int __v;
    @Json(name = "seo_description")
    private String seo_description;
    @Json(name = "seo_title")
    private String seo_title;
    @Json(name = "header_type")
    private String header_type;
    @Json(name = "publish_date")
    private String publish_date;
    @Json(name = "model")
    private String model;
    @Json(name = "horizontal_cover_image")
    private String horizontal_cover_image;
    @Json(name = "propertyTag")
    private Tags propertyTag;
    @Json(name = "orderIndex")
    private int orderIndex;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTimestamp_added() {
        return timestamp_added;
    }

    public void setTimestamp_added(String timestamp_added) {
        this.timestamp_added = timestamp_added;
    }

    public String getTimestamp_modified() {
        return timestamp_modified;
    }

    public void setTimestamp_modified(String timestamp_modified) {
        this.timestamp_modified = timestamp_modified;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getHorizontalImageCover() {
        return horizontalImageCover;
    }

    public void setHorizontalImageCover(String horizontalImageCover) {
        this.horizontalImageCover = horizontalImageCover;
    }

    public CategoryId getCategory_id() {
        return category_id;
    }

    public void setCategory_id(CategoryId category_id) {
        this.category_id = category_id;
    }

    public CategoryId getGroup_id() {
        return group_id;
    }

    public void setGroup_id(CategoryId group_id) {
        this.group_id = group_id;
    }

    public String getAction_button_text() {
        return action_button_text;
    }

    public void setAction_button_text(String action_button_text) {
        this.action_button_text = action_button_text;
    }

    public String getAction_button_link() {
        return action_button_link;
    }

    public void setAction_button_link(String action_button_link) {
        this.action_button_link = action_button_link;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getSeo_description() {
        return seo_description;
    }

    public void setSeo_description(String seo_description) {
        this.seo_description = seo_description;
    }

    public String getSeo_title() {
        return seo_title;
    }

    public void setSeo_title(String seo_title) {
        this.seo_title = seo_title;
    }

    public String getHeader_type() {
        return header_type;
    }

    public void setHeader_type(String header_type) {
        this.header_type = header_type;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getHorizontal_cover_image() {
        return horizontal_cover_image;
    }

    public void setHorizontal_cover_image(String horizontal_cover_image) {
        this.horizontal_cover_image = horizontal_cover_image;
    }

    public Tags getPropertyTag() {
        return propertyTag;
    }

    public void setPropertyTag(Tags propertyTag) {
        this.propertyTag = propertyTag;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }
}
