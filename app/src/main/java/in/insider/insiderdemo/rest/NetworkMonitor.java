package in.insider.insiderdemo.rest;

public interface NetworkMonitor {
    boolean isConnected();
}
