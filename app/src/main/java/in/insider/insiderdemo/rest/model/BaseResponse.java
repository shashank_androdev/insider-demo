package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.List;
import java.util.Map;

public class BaseResponse {
    @Json(name = "tags")
    private String[] tags;
    @Json(name = "groups")
    private String[] groups;
    @Json(name = "filters")
    private Filters filters;
    @Json(name = "sorters")
    private Sorters sorters;
    @Json(name = "list")
    private BaseList list;
    @Json(name = "picks")
    private Picks picks;
    @Json(name = "popular")
    private List<MasterListDetail> popular;
    @Json(name = "text")
    private Map<String,DisplayDetails> text;
    @Json(name = "featured")
    private List<MasterListDetail> featured;
    @Json(name = "dates")
    private Dates dates;
    @Json(name = "banners")
    private List<MasterListDetail> banners;

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String[] getGroups() {
        return groups;
    }

    public void setGroups(String[] groups) {
        this.groups = groups;
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public Sorters getSorters() {
        return sorters;
    }

    public void setSorters(Sorters sorters) {
        this.sorters = sorters;
    }

    public BaseList getList() {
        return list;
    }

    public void setList(BaseList list) {
        this.list = list;
    }

    public Picks getPicks() {
        return picks;
    }

    public void setPicks(Picks picks) {
        this.picks = picks;
    }

    public List<MasterListDetail> getPopular() {
        return popular;
    }

    public void setPopular(List<MasterListDetail> popular) {
        this.popular = popular;
    }

    public Map<String, DisplayDetails> getText() {
        return text;
    }

    public void setText(Map<String, DisplayDetails> text) {
        this.text = text;
    }

    public List<MasterListDetail> getFeatured() {
        return featured;
    }

    public void setFeatured(List<MasterListDetail> featured) {
        this.featured = featured;
    }

    public Dates getDates() {
        return dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public List<MasterListDetail> getBanners() {
        return banners;
    }

    public void setBanners(List<MasterListDetail> banners) {
        this.banners = banners;
    }
}
