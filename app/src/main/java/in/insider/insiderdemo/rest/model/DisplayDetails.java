package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class DisplayDetails {
    @Json(name = "seo_title")
    private String seo_title;
    @Json(name = "seo_description")
    private String seo_description;
    @Json(name = "colour")
    private String colour;
    @Json(name = "pick_title")
    private String pick_title;
    @Json(name = "pick_description")
    private String pick_description;
    @Json(name = "all_title")
    private String all_title;
    @Json(name = "all_description")
    private String all_description;
    @Json(name = "link_type")
    private String link_type;
    @Json(name = "link_slug")
    private String link_slug;

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public String getLink_slug() {
        return link_slug;
    }

    public void setLink_slug(String link_slug) {
        this.link_slug = link_slug;
    }

    public String getSeo_title() {
        return seo_title;
    }

    public void setSeo_title(String seo_title) {
        this.seo_title = seo_title;
    }

    public String getSeo_description() {
        return seo_description;
    }

    public void setSeo_description(String seo_description) {
        this.seo_description = seo_description;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getPick_title() {
        return pick_title;
    }

    public void setPick_title(String pick_title) {
        this.pick_title = pick_title;
    }

    public String getPick_description() {
        return pick_description;
    }

    public void setPick_description(String pick_description) {
        this.pick_description = pick_description;
    }

    public String getAll_title() {
        return all_title;
    }

    public void setAll_title(String all_title) {
        this.all_title = all_title;
    }

    public String getAll_description() {
        return all_description;
    }

    public void setAll_description(String all_description) {
        this.all_description = all_description;
    }
}
