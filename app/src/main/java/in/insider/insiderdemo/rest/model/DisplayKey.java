package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class DisplayKey {
    @Json(name = "display")
    private String display;
    @Json(name = "key")
    private String key;
    @Json(name = "type")
    private String type;
    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
