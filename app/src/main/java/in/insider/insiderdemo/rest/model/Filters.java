package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class Filters {
    @Json(name = "Events")
    private FiltersCatg events;
    @Json(name = "Food")
    private FiltersCatg food;
    @Json(name = "Travel")
    private FiltersCatg travel;
    @Json(name = "Workshops")
    private FiltersCatg workshops;
    @Json(name = "BARgain")
    private FiltersCatg bARgain;
    @Json(name = "Festivals")
    private FiltersCatg festivals;
    @Json(name = "Theatre")
    private FiltersCatg theatre;

    public FiltersCatg getEvents() {
        return events;
    }

    public void setEvents(FiltersCatg events) {
        this.events = events;
    }

    public FiltersCatg getFood() {
        return food;
    }

    public void setFood(FiltersCatg food) {
        this.food = food;
    }

    public FiltersCatg getTravel() {
        return travel;
    }

    public void setTravel(FiltersCatg travel) {
        this.travel = travel;
    }

    public FiltersCatg getWorkshops() {
        return workshops;
    }

    public void setWorkshops(FiltersCatg workshops) {
        this.workshops = workshops;
    }

    public FiltersCatg getbARgain() {
        return bARgain;
    }

    public void setbARgain(FiltersCatg bARgain) {
        this.bARgain = bARgain;
    }

    public FiltersCatg getFestivals() {
        return festivals;
    }

    public void setFestivals(FiltersCatg festivals) {
        this.festivals = festivals;
    }

    public FiltersCatg getTheatre() {
        return theatre;
    }

    public void setTheatre(FiltersCatg theatre) {
        this.theatre = theatre;
    }
}
