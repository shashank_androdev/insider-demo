package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.List;

public class FiltersCatg {
    @Json(name = "show")
    private List<DisplayKey> show;

    public List<DisplayKey> getShow() {
        return show;
    }

    public void setShow(List<DisplayKey> show) {
        this.show = show;
    }
}
