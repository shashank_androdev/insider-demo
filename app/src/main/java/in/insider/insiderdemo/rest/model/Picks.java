package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

import java.util.Map;

public class Picks {
    @Json(name = "masterList")
    private Map<String,PicksListDetail> masterList;
    @Json(name = "groupwiseList")
    private Map<String,String[]> groupwiseList;
    @Json(name = "categorywiseList")
    private Map<String,String[]> categorywiseList;

    public Map<String, PicksListDetail> getMasterList() {
        return masterList;
    }

    public void setMasterList(Map<String, PicksListDetail> masterList) {
        this.masterList = masterList;
    }

    public Map<String, String[]> getGroupwiseList() {
        return groupwiseList;
    }

    public void setGroupwiseList(Map<String, String[]> groupwiseList) {
        this.groupwiseList = groupwiseList;
    }

    public Map<String, String[]> getCategorywiseList() {
        return categorywiseList;
    }

    public void setCategorywiseList(Map<String, String[]> categorywiseList) {
        this.categorywiseList = categorywiseList;
    }
}
