package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class Dates {
    @Json(name = "today")
    private String today;
    @Json(name = "tomorrow")
    private String tomorrow;
    @Json(name = "weekend")
    private String weekend;
    @Json(name = "next_weekend")
    private String next_weekend;

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getTomorrow() {
        return tomorrow;
    }

    public void setTomorrow(String tomorrow) {
        this.tomorrow = tomorrow;
    }

    public String getWeekend() {
        return weekend;
    }

    public void setWeekend(String weekend) {
        this.weekend = weekend;
    }

    public String getNext_weekend() {
        return next_weekend;
    }

    public void setNext_weekend(String next_weekend) {
        this.next_weekend = next_weekend;
    }
}
