package in.insider.insiderdemo.rest.model;

import com.squareup.moshi.Json;

public class CategoryId {
    @Json(name = "_id")
    private String _id;
    @Json(name = "name")
    private String name;
    @Json(name = "icon_img")
    private String icon_img;
    @Json(name = "display_details")
    private DisplayDetails display_details;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public void setIcon_img(String icon_img) {
        this.icon_img = icon_img;
    }

    public DisplayDetails getDisplay_details() {
        return display_details;
    }

    public void setDisplay_details(DisplayDetails display_details) {
        this.display_details = display_details;
    }
}
