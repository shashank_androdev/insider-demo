package in.insider.insiderdemo.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import in.insider.insiderdemo.AppExecutors;
import in.insider.insiderdemo.rest.ApiInterface;
import in.insider.insiderdemo.rest.model.ApiResponse;
import in.insider.insiderdemo.rest.model.BaseResponse;
import in.insider.insiderdemo.rest.model.Resource;
import timber.log.Timber;

public class EventRepository {
    private final ApiInterface apiInterface;
    private final AppExecutors appExecutors;
    @Inject
    public EventRepository(AppExecutors appExecutors, ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
        this.appExecutors = appExecutors;
    }
    public LiveData<Resource<BaseResponse>> loadData(int norm, String filterBy,String city) {
        return new NetworkBoundResource<BaseResponse>(appExecutors) {
            @Override
            protected void saveCallResult(@NonNull BaseResponse contributors) {

            }

            @Override
            protected boolean shouldFetch(@Nullable BaseResponse data) {
                return data == null ;
            }

            @NonNull
            @Override
            protected LiveData<BaseResponse> loadFromDb() {
                return null;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BaseResponse>> createCall() {
                return apiInterface.getResponse(norm, filterBy,city);
            }
        }.asLiveData();
    }
}
