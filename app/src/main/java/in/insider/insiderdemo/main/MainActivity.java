package in.insider.insiderdemo.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import in.insider.insiderdemo.MainApplication;
import in.insider.insiderdemo.R;
import in.insider.insiderdemo.binding.ActivityDataBindingComponent;
import in.insider.insiderdemo.databinding.ActivityMainBinding;
import in.insider.insiderdemo.rest.model.BaseResponse;
import in.insider.insiderdemo.rest.model.MasterListDetail;
import in.insider.insiderdemo.rest.model.Resource;
import in.insider.insiderdemo.utils.AutoClearedValue;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    @Inject
    ViewModelProvider.Factory viewModelFactory;


    private MainViewModel mainViewModel;

    DataBindingComponent dataBindingComponent = new ActivityDataBindingComponent(this);
    AutoClearedValue<ActivityMainBinding> binding;
    AutoClearedValue<EventAdapter> adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
        ActivityMainBinding dataBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_main);
        dataBinding.setRetryCallback(() -> mainViewModel.retry());
        binding = new AutoClearedValue<>((MainApplication) getApplication(), dataBinding);
        setContentView(dataBinding.getRoot());
        mainViewModel.setReq(1, "go-out", "delhi");
        LiveData<Resource<BaseResponse>> data = mainViewModel.getData();
        data.observe(this, baseResponseResource -> {
            binding.get().setResource(baseResponseResource);
            binding.get().executePendingBindings();
        });

        EventAdapter adapter = new EventAdapter(dataBindingComponent,
                event -> {
                });
        this.adapter = new AutoClearedValue<>((MainApplication) getApplication(), adapter);
        binding.get().eventList.setAdapter(adapter);
        initEventList(mainViewModel);
    }

    private void initEventList(MainViewModel viewModel) {
        viewModel.getData().observe(this, listResource -> {
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource != null && listResource.data != null) {
                List<MasterListDetail> masterListDetails = new ArrayList<>();
                Map<String, MasterListDetail> map = listResource.data.getList().getMasterList();
                Set<String> keySet = map.keySet();
                Iterator<String> keys = keySet.iterator();
                while (keys.hasNext()) {
                    String key = keys.next();
                    masterListDetails.add(map.get(key));
                }
                adapter.get().replace(masterListDetails);
            } else {
                //noinspection ConstantConditions
                adapter.get().replace(Collections.emptyList());
            }
        });
    }

}
