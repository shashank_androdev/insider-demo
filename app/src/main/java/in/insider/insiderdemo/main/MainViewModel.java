package in.insider.insiderdemo.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

import in.insider.insiderdemo.repository.EventRepository;
import in.insider.insiderdemo.rest.model.BaseResponse;
import in.insider.insiderdemo.rest.model.Resource;
import in.insider.insiderdemo.utils.AbsentLiveData;

public class MainViewModel extends ViewModel {
    final MutableLiveData<Req> req;
    private final LiveData<Resource<BaseResponse>> data;

    @Inject
    public MainViewModel(EventRepository repository) {
        this.req = new MutableLiveData<>();
        data = Transformations.switchMap(req, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadData(input.norm, input.filtertype,input.city);
        });

    }
    public void setReq(int norm,String filterType, String city) {
        Req update = new Req(norm,filterType, city);

        req.setValue(update);
    }
    public void retry() {
        Req current = req.getValue();
        if (current != null && !current.isEmpty()) {
            req.setValue(current);
        }
    }

    public LiveData<Resource<BaseResponse>> getData() {
        return data;
    }
    static class Req {
        public final String filtertype;
        public final String city;
        public final int norm;
        Req(int norm, String filtertype,String city) {
            this.filtertype = filtertype == null ? null : filtertype.trim();
            this.city = city == null ? null : city.trim();
            this.norm=norm;
        }

        boolean isEmpty() {
            return filtertype == null || city == null || filtertype.length() == 0 || city.length() == 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Req repoId = (Req) o;

            if (filtertype != null ? !filtertype.equals(repoId.filtertype) : repoId.filtertype != null) {
                return false;
            }
            return city != null ? city.equals(repoId.city) : repoId.city == null;
        }

        @Override
        public int hashCode() {
            int result = filtertype != null ? filtertype.hashCode() : 0;
            result = 31 * result + (city != null ? city.hashCode() : 0);
            return result;
        }
    }
}
