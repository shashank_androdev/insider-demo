package in.insider.insiderdemo.main;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.insider.insiderdemo.R;
import in.insider.insiderdemo.common.DataBoundListAdapter;
import in.insider.insiderdemo.rest.model.MasterListDetail;
import in.insider.insiderdemo.databinding.EventItemBinding;
import in.insider.insiderdemo.utils.Objects;

public class EventAdapter extends DataBoundListAdapter<MasterListDetail, EventItemBinding> {
    private final DataBindingComponent dataBindingComponent;
    private final EventClickCallback callback;
    public EventAdapter(DataBindingComponent dataBindingComponent,
                        EventClickCallback callback) {
        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
    }

    @Override
    protected EventItemBinding createBinding(ViewGroup parent) {
        EventItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.event_item, parent, false,
                        dataBindingComponent);
        binding.getRoot().setOnClickListener(v -> {
            MasterListDetail contributor = binding.getEvent();
            if (contributor != null && callback != null) {
                callback.onClick(contributor);
            }
        });
        return binding;
    }

    @Override
    protected void bind(EventItemBinding binding, MasterListDetail item) {
        binding.setEvent(item);
    }

    @Override
    protected boolean areItemsTheSame(MasterListDetail oldItem, MasterListDetail newItem) {
        return Objects.equals(oldItem.get_id(), newItem.get_id());
    }

    @Override
    protected boolean areContentsTheSame(MasterListDetail oldItem, MasterListDetail newItem) {
        return Objects.equals(oldItem.get_id(), newItem.get_id())
                && oldItem.getVenue_id() == newItem.getVenue_id();
    }

    public interface EventClickCallback {
        void onClick(MasterListDetail event);
    }
}
