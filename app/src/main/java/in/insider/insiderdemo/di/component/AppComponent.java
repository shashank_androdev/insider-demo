package in.insider.insiderdemo.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import in.insider.insiderdemo.MainApplication;
import in.insider.insiderdemo.di.module.AppModule;
import in.insider.insiderdemo.di.module.MainActivityModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        MainActivityModule.class
})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
    void inject(MainApplication mainApplication);
}
