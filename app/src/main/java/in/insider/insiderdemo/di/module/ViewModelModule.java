package in.insider.insiderdemo.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import in.insider.insiderdemo.di.ViewModelKey;
import in.insider.insiderdemo.main.MainViewModel;
import in.insider.insiderdemo.viewmodel.InsiderViewModelFactory;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindUserViewModel(MainViewModel mainViewModel);
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(InsiderViewModelFactory factory);
}
