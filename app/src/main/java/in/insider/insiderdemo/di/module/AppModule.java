package in.insider.insiderdemo.di.module;

import android.app.Application;
import android.content.Context;

import com.squareup.moshi.Moshi;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.insider.insiderdemo.BuildConfig;
import in.insider.insiderdemo.di.ApplicationContext;
import in.insider.insiderdemo.rest.ApiInterface;
import in.insider.insiderdemo.rest.LiveNetworkMonitor;
import in.insider.insiderdemo.rest.NetworkMonitor;
import in.insider.insiderdemo.utils.Constants;
import in.insider.insiderdemo.utils.LiveDataCallAdapterFactory;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by shashank on 17/07/17.
 */
@Module(includes = ViewModelModule.class)
public class AppModule {
    @Provides
    @Named()
    String provideBaseUrlString() {
        return Constants.BASE_URL;
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    NetworkMonitor provideNetworkMonitor(Application application) {
        return new LiveNetworkMonitor(application);
    }

    @Provides
    @Singleton
    OkHttpClient.Builder provideOkHttpClient(Cache cache, final Application application, final NetworkMonitor networkMonitor) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.cache(cache);
        httpClient.addInterceptor(logging);
        httpClient.readTimeout(180, TimeUnit.SECONDS);
        httpClient.connectTimeout(180, TimeUnit.SECONDS);
        httpClient.writeTimeout(180, TimeUnit.SECONDS);
        return httpClient;
    }

    @Provides
    @Singleton
    Converter.Factory provideMoshiConverter() {
        Moshi moshi = new Moshi.Builder()
                .build();
        return MoshiConverterFactory.create(moshi);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Converter.Factory converter, @Named() String baseUrl, OkHttpClient.Builder okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .client(okHttpClient.build())
                .build();
    }

    @Provides
    @Singleton
    ApiInterface provideApiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

}
