package in.insider.insiderdemo.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

public class ActivityBindingAdapter {
    final Context context;

    @Inject
    public ActivityBindingAdapter(Context context) {
        this.context = context;
    }
    @BindingAdapter("imageUrl")
    public void bindImage(ImageView imageView, String url) {
        Glide.with(context).load(url).into(imageView);
    }
}
