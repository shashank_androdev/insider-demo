package in.insider.insiderdemo.binding;

import android.content.Context;
import android.databinding.DataBindingComponent;

public class ActivityDataBindingComponent implements DataBindingComponent {
    private final ActivityBindingAdapter adapter;

    public ActivityDataBindingComponent(Context context) {
        this.adapter = new ActivityBindingAdapter(context);
    }

    @Override
    public ActivityBindingAdapter getActivityBindingAdapter() {
        return adapter;
    }
}
